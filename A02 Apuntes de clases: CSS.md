# CSS: Medidas, fondos y fuentes

## [Para aplicar CSS](https://www.w3schools.com/css/css_howto.asp)

- External CSS

```html
<head>
  <link rel="stylesheet" href="mystyle.css" />
</head>
```

- Internal CSS

```html
<head>
  <style>
    body {
      background-color: linen;
    }
  </style>
</head>
```

- Inline CSS

```html
<body>
  <h1 style="color:blue;text-align:center;">This is a heading</h1>
  <p style="color:red;">This is a paragraph.</p>
</body>
```

## Padre > Hijo

Es una buena práctica incluir la etiqueta padre:

```html
<section>
  <article>
    <h2></h2>
  </article>
</section>
```

```css
section article {
  color: red;
}
```

## Selección / asignación de estilos

```css
h1 {
  propiedad: valor;
}
.clase {
  propiedad: valor;
}
#id {
  propiedad: valor;
}
```

### Reset CSS

Como algunos navegadores modifican algunas propiedades por defecto, se estila "resetear" el CSS para luego aplicarle los atributos, propiedades, etc.. Por ejemplo:

```css
* {
  margin: 0;
  padding: 0;
}
```

## class

Pueden ser varias clases para una misma etiqueta y se pueden reutilizar:

```html
<img src="" class="bordesRedondeados imgChica" />
```

```css
.bordesRedondeados {
  color: green;
}
```

## id

No se pueden reutilizar.

- para entender: atributo id es para frontend, name para backend.

```css
#idEtiqueta {
  color: green;
}
```

## Estilos de texto

- [A complete collection of web safe CSS font stacks](https://www.cssfontstack.com/)  
  Obviamente no se pueden aplicar todas las propiedades juntas y va SÓLO una por línea, sólo lo hago a los efectos de simplificar estas notas:

```css
p {
    font-style: normal / italic;
    font-weight: bold / normal;
    font-size: 20px /: 130%;
    font-family: Impact, sans-serif / "Rubik", sans-serif;
    text-align: center / right;
    text-transform: uppercase / capitalize;
    line-height: 1.6;
    text-decoration: none / underline / line-through;
    letter-spacing: 5px; word-spacing: 30px;
}
```

## Estilos de listas

```css
ol {
  list-style-type: upper-latin;
  list-style-type: upper-roman;
  list-syle-image: url("https://example.com/icon.png");
  list-style-position: outside / inside;
}
```

## Estilos para fondos

```css
.fondo {
  background-color: yellow;
  background-image: url(https://images4.alphacoders.com/166/166554.jpg);
  background-repeat: repeat-x, repeat-y / no-repeat / space;
  background-size: cover / contain;
}
```

### Unidades de medida

- px: pixels
- rem: relativa a la etiqueta (margin, padding, font-size)
- porcentaje: 16px = 100% (width)
- viewport: para uso responsive (height)

## Tipografías

- [Repositorio de fuentes libres de Google](https://fonts.google.com/)  
  Primero se importa en el HTML (se pueden seleccionar variables):

```html
<style>
  @import url("https://fonts.googleapis.com/css2?family=Noto+Sans&display=swap");
</style>
```

Luego se incorpora al stylesheet:

```css
h1 {
  font-family: "Noto Sans", sans-serif;
}
```

## Box Modeling
* pendiente, clase 05 Coder

## Flexbox
- [Flexbox en W3S](https://www.w3schools.com/css/css3_flexbox.asp)
- [Jugar con flexbox froggy](https://flexboxfroggy.com/#es)
- [Jugar con flexbox defense](http://www.flexboxdefense.com/)
- [A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

