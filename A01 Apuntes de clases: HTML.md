# Diseño Web: Introducción HTML

## Principios
- navegabilidad
- interactividad
- arquitectura de la información

## Lenguajes
- HTML (etiqueta, atributo, estructura)
- CSS (estilo, reglas, medidas, fuente)

## Prototipado
- sketch
- wireframe (qué, dónde, cómo)
- mockup (representación estática)
- prototipo interactivo (representación navegable)
- diseño final

### Tips
- arquitectura de la información: agrupar, jerarquizar, rotular
- testear
- tipografía (16px promedio, 10 palabras por línea)
- [paleta cromática](https://www.colourlovers.com/)
- benchmarking

## Etiquetas
- apertura y cierre
- el contenido es lo que se formatea
- si se anidan, se cierran en el orden inverso a la apertura
- algunas no se cierran (elementos como imágenes como ```<hr>``` o ```<br>```, líneas, etc)
```html
<title>Diseño web</title>
<h1>Bienvenides a Diseño Web</h1>
```

### Atributos
- características diferentes entre las etiquetas
- en la apertura y entre comillas
- una o varias, separadas con espacio
```html
<h1 align="center">Bienvenides a Diseño Web</h1>
```

## Tipos
- elementos de bloque: sin CSS, ocupan el 100% del ancho, uno debajo del otro
- elementos de línea: ancho según contenido, uno al lado del otro

## Estructura básica
```<!DOCTYPE html>```: definición del tipo de documento  
```<html></html>```: etiqueta "vertebral" del sitio, define el estándar  
```<head></head>```: info privada para el navegador, title, meta, import de archivos y demás  
```<title></title>```: título de la página, lo muestra el navegador  
```<meta>```: info de la página, tags, autor, idioma, codificación, etc.  
```<body></body>```: contenido del sitio  

### Etiquetas semánticas
[Ejemplo de estructura](https://drive.google.com/file/d/1DiXv1dudk-lDfBJ9eFq3TvQ92vSkCiVr/view?usp=sharing)  
```<header></header>```: encabezado  
```<nav></nav>```: menú, navegación  
```<section></section>```: sección  
```<article></article>```: contenido autónomo de una sección  
```<aside></aside>```: barra lateral, ads, tags, etc.  
```<footer></footer>```: pie de página  

## Listas
- agrupan elementos con un significado común / conjunto
- por ejemplo el menú de navegación es una serie de palabras (índice), que juntas tienen sentido == listas
-  dentro de una lista sólo se aceptan list-items, y dentro de un list-item se acepta cualquier elemento
- ordenadas (números) ```<ol></ol>```, no ordenadas (viñetas) ```<ul></ul>``` y de definición ```<li></li>```
```html
<!-- Ver 03_Desafío_listas.html -->
<!-- Lista para menú de navegación -->
<ul>
    <li>Home</li>
    <li>Quiénes somos</li>
    <li>Trinchera feminista</li>
</ul>
<!-- Listas de definición -->
<dl> <!-- lista de definiciones -->
  <dt>SGML</dt> <!-- término a definir -->
  <dd>Metalenguaje para la definición de otros lenguajes de marcado</dd> <!-- definición -->
</dl>
```

## Tablas
- conjunto de celdas organizadas, para representar información tabulada (filas y columnas).
- en HTML4 se usaban para maquetar, hasta que llegó CSS y se pudrió.
- múltiples etiquetas: ```<table></table>```, ```<tr></tr>``` para celdas, ```<th></th>``` Fecha, ```<td></td>``` Mayo 18, 1983.
- admiten 3 atributos de diseño: ```border```, ```cellpadding``` (espacio entre borde y contenido), ```cellspacing``` (distnacia entre celdas y margen externo).

## Formularios
- ver [05_Práctica_formularios.html](https://gitlab.com/diegobollini/curso-desarrollo-web/-/blob/master/05_Pr%C3%A1ctica_formularios.html)
- etiqueta ```<form>```
- inputs: text, email, password, date
- atributos: action, method, value, placeholder, required.
- requiere 3:
    - ```action```: documento que recibe y procesa datos, por defecto donde esté el form, debería ser PHP, ASP, JSP
    - ```method```: envío de datos, por defecto get [en la URL] o post [invisible en los encabezados]
    - ```enctype```: codificación. Cuando es post (para enviar archivos):
        - ```application/x-www-form-urlencoded```: valor por defecto
        - ```multipart/form-data```: si es input + type = file
        - ```text/plain```: HTML5
- HTML NO es un lenguaje de programación (no es meme).

### Text input
- una sola línea
- contraseñas (contenido invisible)
- multilínea
```html
<!-- atributo "name" -->
<input>: text, email, password
<textarea></textarea>
```

### Botones
- son etiquetas ```<input>```, con distintos ```<type>```, siempre dentro del ```<form>```
- 3 tipos:
    - enviar datos al archivo indicado en Action
    - vaciar y resetear campos
    - sin acción, para usar con Javascript

#### Atributo "value"
- etiqueta del botón, que muestran los navegadores:
```html
<form action="">
    <input type="submit" value="Enviar formulario">
    <input type="reset" value="Limpiar formulario">
    <input type="button" value="Sin acción">
</form>
```

## Controles de selección
- listas predefinidas (usando atributo "value")
- grupos:
    - radio buttons: sólo una opción posible
    - checkbox: una, varias o todas
    - combo-box: una opción del desplegable

### Etiqueta <label>
- definen a cada elemento de un form, importantes para que sean accesibles.
- su principal atributo es "for", referencia "label" con su elemento del form.
    - "for" debe ser igual a "id" o "name"
```html
<form action="">
    <label for="nombre_apellido">Nombre y apellido</label>
    <input type="text" name="nombre_apellido" id="">
</form>
```

### Conjunto de campo
- ```<fieldset>```: grupos de elementos con un mismo propósito
- ```<legend>```: define el propósito de "fieldset"
```html
<form action="">
    <fieldset>
        <legend>Talle de remera</legend>
        <!-- Acá van todos los elementos del form-->
    </fieldset>
</form>
```

## Enlaces
- se utiliza la etiqueta ```<a>``` con el atributo "href" (destino del link)
- pueden ser Relativos, Absolutos e Internos
```html
<!-- Relativos: páginas dentro del mismo proyecto / directorio -->
<a href="quienes_somos.html">Quiénes somos</a>
<a href="img/logo.jpg">Logo</a>

<!-- Absolutos: fuera del sitio, con URL completa -->
<a href="https://ckck.bandcamp.com/">CKCK</a>

<!-- Internos: hacia secciones dentro de la misma u otra página -->
<a href="#footer">Ir al pie de página</a>
<a href="contacto.html#formulario">Formulario de contacto</a>
...
<footer id="footer"></footer>

<!-- Linkeando desde imágenes -->
<a href="https://coderhouse.com/cursos.html#fronted">
    <img src="img/logo_coderhouse.png" alt="coderhouse"/>
</a>
```
