# Curso Desarrollo Web

Curso de Desarrollo Web en [CoderHouse](https://www.coderhouse.com/online/desarrollo-web-online)
- [Plataforma](https://plataforma.coderhouse.com)
- [Repositorio de herramientas y recursos](https://www.notion.so/Repositorio-de-Contenidos-Desarrollo-301736845df840af918a1830eb0d3f88)

## Objetivos
- introducción desarrollo web
- crear página web responsive
- buenas prácticas, SEO, mobile first
- dinámicas y herramientas

## Herramientas para workflow de desarrollo web
- Codium:
    - extensions CodeSnap, GitLens, auto close tag, browser preview, color picker, dracula theme, indent rainbow, live server, prettier
- Balsamiq (maquetación)
    - [cloud](https://balsamiq.cloud/slsqc34/p18k7n3/r2278)
    - [lalibrecf.com.ar project](https://gitlab.com/diegobollini/proyecto-web-la-libre/-/blob/master/Maqueta%2001%20lalibre-web.pdf)
- GitLab
    - [Repositorio del curso](https://gitlab.com/diegobollini/curso-desarrollo-web)
    - [Repositorio proyecto La Libre](https://gitlab.com/diegobollini/proyecto-web-la-libre)
    - [CI/CD](https://diegobollini.gitlab.io/proyecto-web-la-libre/)

## Otros recursos
- [Referencia de Elementos HTML](https://developer.mozilla.org/es/docs/Web/HTML/Elemento)
- [HTML Element Reference](https://www.w3schools.com/tags/default.asp)
- [Debate sobre nav element](http://html5doctor.com/nav-element/)
- [Curso de Desarrollo Web Inicial de Franco Di Leo](https://www.youtube.com/playlist?list=PLtKI8MF06ilBBbF_8KAcceCbFD4ZUsbrs)